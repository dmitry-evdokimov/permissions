<?php

namespace Permissions\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\MutableCreationOptionsInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcRbac\Exception;
use Permissions\Model\RoleProvider;

/**
 * Class RoleProviderFactory
 * @package Permissions\Factory
 */
class RoleProviderFactory implements FactoryInterface, MutableCreationOptionsInterface
{
    protected $options = [];

	/**
     * @param array $options
     */
    public function setCreationOptions(array $options)
    {
        $this->options = $options;
    }

	/**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return RoleProvider
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();

        return new RoleProvider(
            $this->options,
            $services->get('QueryBuilderModel')
        );
    }
}