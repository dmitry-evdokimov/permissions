<?php

namespace Permissions\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Permissions\Model\PermissionModel;

/**
 * Class PermissionModelFactory
 * @package Permissions\Factory
 */
class PermissionModelFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return PermissionModel
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new PermissionModel(
            $serviceLocator->get('QueryBuilderModel')
        );
    }
}