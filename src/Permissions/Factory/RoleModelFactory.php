<?php

namespace Permissions\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Permissions\Model\RoleModel;

/**
 * Class RoleModelFactory
 * @package Permissions\Factory
 */
class RoleModelFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return RoleModel
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new RoleModel(
            $serviceLocator->get('QueryBuilderModel')
        );
    }
}