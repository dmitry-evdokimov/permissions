<?php

namespace Permissions\Model;

use Doctrine\ODM\MongoDB\Id\UuidGenerator;
use ZfcRbac\Role\RoleProviderInterface;
use Rbac\Role\RoleInterface;
use Rbac\Role\HierarchicalRole;
use Rbac\Role\Role;
use OAuth2\Request as OAuth2Request;

/**
 * Class RoleProvider
 * @package Permissions\Model
 */
class RoleProvider implements RoleProviderInterface
{
    private $roles = [];
    private $rolesConfig = [];
    private $queryBuilderModel;
    private $roleCache = [];
    private $uuidGenerator;
    private $currentUser;
    private $instanceUrl;

	/**
     * RoleProvider constructor.
     *
     * @param array $rolesConfig
     * @param $queryBuilderModel
     */
    public function __construct(array $rolesConfig, $queryBuilderModel)
    {
        $this->rolesConfig = $rolesConfig;
        $this->queryBuilderModel = $queryBuilderModel;
        $this->uuidGenerator = new UuidGenerator();
        $this->currentUser = null;
        $this->instanceUrl = null;

        $globals = OAuth2Request::createFromGlobals();
        $headers = $globals->headers;

        if (!empty($headers['AUTHORIZATION'])) {
            $accessToken = substr($headers['AUTHORIZATION'], strlen('Bearer '));
        } else {
            $accessToken = null;
        }

        $token = $this->queryBuilderModel->fetch('User\Entity\UserToken', ['access_token' => $accessToken]);

        if (!empty($token)) {
            $tokenData = $token->getData();
            if (!empty($tokenData)) {
                $this->currentUser = $this->queryBuilderModel->fetch(
                    'User\Entity\User',
                    ['username' => $tokenData['user_id']]
                );
                if ($this->currentUser !== null) {
                    $this->instanceUrl = $this->currentUser->getInstanceUrl();
                }
            }
        }
    }

    /**
     * Clears the role cache
     *
     * @return void
     */
    public function clearRoleCache()
    {
        $this->roleCache = [];
    }

	/**
     * @param array $roleNames
     *
     * @return array
     */
    public function getRoles(array $roleNames)
    {
        $key = implode($roleNames);

        $roles = [];

        foreach ($roleNames as $roleName) {
            if (is_string($roleName)) {
                if ($this->uuidGenerator->isValid($roleName)) {
                    $roleUuid = $roleName;
                    $roleEntity = $this->queryBuilderModel->fetch(
                        'Permissions\Entity\Role',
                        ['uuid' => $roleUuid]
                    );
                } else {
                    $roleCode = $roleName;
                    $roleEntity = $this->queryBuilderModel->fetch(
                        'Permissions\Entity\Role',
                        ['code' => $roleCode, 'instanceUrl' => $this->instanceUrl]
                    );
                }

                $role = null;

                if (!empty($roleEntity)) {
                    $roleId = $roleEntity->getCode();
                    if (!$roleId) {
                        $roleId = $roleEntity->getUuid();
                    }
                    $role = new Role($roleId);

                    $permissions = $roleEntity->getPermissions();

                    foreach ($permissions as $permission) {
                        $role->addPermission($permission);
                    }

                    $permissions = $this->getStaticPermissions($roleId);

                    if (!empty($permissions)) {
                        foreach ($permissions as $permission) {
                            $role->addPermission($permission);
                        }
                    }
                } else {
                    $role = $this->getRole($roleName);
                }

                if (!empty($role)) {
                    $roles[] = $role;
                }
            }
        }

        $this->roleCache[$key] = $roles;
        return $roles;
    }

    /**
     * Get role by role name
     *
     * @param $roleName
     *
     * @return Role
     */
    protected function getRole($roleName)
    {
        if (isset($this->roles[$roleName])) {
            return $this->roles[$roleName];
        }

        // If no config, return null
        if (!isset($this->rolesConfig[$roleName])) {
            return null;
        }

        $roleConfig = $this->rolesConfig[$roleName];

        if (isset($roleConfig['children'])) {
            $role = new HierarchicalRole($roleName);
            $childRoles = (array)$roleConfig['children'];
            foreach ($childRoles as $childRole) {
                $childRole = $this->getRole($childRole);
                $role->addChild($childRole);
            }
        } else {
            $role = new Role($roleName);
        }

        $permissions = isset($roleConfig['permissions']) ? $roleConfig['permissions'] : [];
        foreach ($permissions as $permission) {
            $role->addPermission($permission);
        }

        $this->roles[$roleName] = $role;

        return $role;
    }

	/**
     * @param $roleName
     *
     * @return array|null
     */
    protected function getStaticPermissions($roleName)
    {
        // If no config, return null
        if (!isset($this->rolesConfig[$roleName])) {
            return null;
        }

        $roleConfig = $this->rolesConfig[$roleName];
        $permissions = isset($roleConfig['permissions']) ? $roleConfig['permissions'] : [];

        return $permissions;
    }
}
