<?php

namespace Permissions\Model;

/**
 * Class PermissionModel
 * @package Permissions\Model
 */
class PermissionModel
{
    protected $queryBuilderModel;

    /**
     * @param $queryBuilderModel
     */
    public function __construct($queryBuilderModel)
    {
        $this->queryBuilderModel = $queryBuilderModel;
    }

    /**
     * @param $data
     * @param null $uuid
     *
     * @return array|null
     */
    public function createOrUpdate($data, $uuid = null)
    {
        if (!empty($uuid)) {
            // only 'name' and 'description' can be changed
            $data = array_intersect_key($data, array_flip(['name', 'description']));

            $resource = $this->queryBuilderModel->fetch('Permissions\Entity\Permission', ['uuid' => $uuid])->getData();
        } else {
            $resource = [];
        }

        $resource = array_merge($resource, $data);

        return $this->queryBuilderModel->createOrUpdate('Permissions\Entity\Permission', $resource, $uuid);
    }

    /**
     * @param $searchArray
     *
     * @return mixed
     */
    public function fetch($searchArray, $queryParams = array())
    {
        return $this->queryBuilderModel->fetch('Permissions\Entity\Permission', $searchArray, $queryParams);
    }

    /**
     * @param $searchArray
     *
     * @return mixed
     */
    public function fetchAll($searchArray, $queryParams = array())
    {
        return $this->queryBuilderModel->fetchAll('Permissions\Entity\Permission', $searchArray, $queryParams);
    }

    /**
     * @param $uuid
     *
     * @return mixed
     */
    public function delete($uuid)
    {
        return $this->queryBuilderModel->delete('Permissions\Entity\Permission', array('uuid' => $uuid));
    }

	/**
     * @param $instanceUrl
     * @param $permission
     * @param $department
     *
     * @return bool
     */
    public function userExistByPermission($instanceUrl, $permission, $department)
    {
        $roles = $this->queryBuilderModel->fetchAll(
            'Permissions\Entity\Role',
            ['instanceUrl' => $instanceUrl, 'permissions' => $permission]
        );

        $userRoles = [];
        foreach ($roles as $role) {
            if (empty($role->getCode())) {
                $userRoles[] = $role->getUuid();
            } else {
                $userRoles[] = $role->getCode();
            }
        }

        $usersCount = 0;
        if (!empty($userRoles)) {
            $userProfile = $this->queryBuilderModel->fetchAll('User\Entity\UserProfile', ['department' => $department]);
            $idsArray = [];
            foreach ($userProfile as $d) {
                array_push($idsArray, new \MongoId($d->getId()));
            }
            if (!empty($idsArray)) {
                $usersCount = $this->queryBuilderModel->count(
                    'User\Entity\User',
                    ['instanceUrl' => $instanceUrl, 'roles' => $userRoles, 'userProfile.$id' => $idsArray]
                );
            }
        }

        return ($usersCount > 0);
    }
}
