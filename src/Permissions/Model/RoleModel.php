<?php

namespace Permissions\Model;

/**
 * Class RoleModel
 * @package Permissions\Model
 */
class RoleModel
{
    protected $queryBuilderModel;

    /**
     * @param $queryBuilderModel
     */
    public function __construct($queryBuilderModel)
    {
        $this->queryBuilderModel = $queryBuilderModel;
    }

    /**
     * @param $data
     * @param null $uuid
     *
     * @return array|null
     */
    public function createOrUpdate($data, $uuid = null)
    {
        if (!empty($uuid)) {
            $resource = $this->queryBuilderModel->fetch('Permissions\Entity\Role', ['uuid' => $uuid])->getData();
        } else {
            $resource = [];
        }

        if(isset($data['code'])) {
            unset($data['code']);
        }

        $resource = array_merge($resource, $data);

        if (!isset($resource['permissions']) || empty($resource['permissions'])) {
            $resource['permissions'] = [];
        }

        return $this->queryBuilderModel->createOrUpdate('Permissions\Entity\Role', $resource, $uuid);
    }

    /**
     * @param $searchArray
     *
     * @return mixed
     */
    public function fetch($searchArray, $queryParams = [])
    {
        return $this->queryBuilderModel->fetch('Permissions\Entity\Role', $searchArray, $queryParams);
    }

    /**
     * @param $searchArray
     *
     * @return mixed
     */
    public function fetchAll($searchArray, $queryParams = [])
    {
        return $this->queryBuilderModel->fetchAll('Permissions\Entity\Role', $searchArray, $queryParams);
    }

    /**
     * @param $uuid
     *
     * @return mixed
     */
    public function delete($uuid)
    {
        return $this->queryBuilderModel->delete('Permissions\Entity\Role', array('uuid' => $uuid));
    }
}
