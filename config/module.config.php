<?php

namespace Permissions;

return array(
    'controllers' => array(
        'invokables' => array(
        ),
    ),
    'service_manager' => array(
        'aliases' => array(
        ),
        'factories' => array(
            'PermissionModel' => 'Permissions\Factory\PermissionModelFactory',
            'RoleModel' => 'Permissions\Factory\RoleModelFactory',
            'RoleProvider' => 'Permissions\Factory\RoleProviderFactory',
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'odm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    )
);
